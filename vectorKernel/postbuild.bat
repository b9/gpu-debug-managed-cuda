set cuda=%1
set project=%3
set devroot=%2
set devroot=%devroot:Common7\IDE\=%
"%cuda%\bin\nvcc.exe" -gencode=arch=compute_30,code=\"sm_30,compute_30\" --use-local-env --cl-version 2015 -ccbin "%devroot%VC\bin\x86_amd64" -x cu  -I%cuda%\include  -G   --keep-dir x64\Debug -maxrregcount=0  --machine 64 -ptx -cudart static  -o %project%..\vectorAdd\vectorAdd_x64.ptx "%project%kernel.cu"
