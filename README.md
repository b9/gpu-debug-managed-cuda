## How to GPU debug ManagedCuda

ManagedCuda (https://kunzmi.github.io/managedCuda/) is a great way to launch cuda kernels from C#.
However ManagedCuda typically launches kernels loaded from ptx modules and ptx modules are not
easily debuggable by the Nsight gpu debugger. This is an example Visual Studio solution demonstrating
how a kernel can be built both as a cuda C++ executable and as a cuda ptx module in order to be gpu
debuggable.

---

To CPU debug code as normal:

1. Make sure the vectorAdd C# project is set as start up project
2. Set a brake point in Program.cs
3. Right click the Solution in the Solution Explorer and rebuild the solution
3. Click Start or hit F5

---

To GPU debug code using Nsight:

1. Set vectorKernel as start up project
2. Set a brake point in gpu code for instance in the Calculus class
3. Choose the menu Nsight and the option Start CUDA debugging

---
